/*
 * @Date: 2022-06-11 21:14:48
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-24 15:41:20
 * @FilePath: \taro-music-learn\src\app.tsx
 * @Description: something
 */
import React from "react";
import rootStore from "./store";
import { Provider } from "react-redux";
import "./app.scss";

const App: React.FC = ({ children }) => {
  return <Provider store={rootStore.store}>{children}</Provider>;
};

export default App;
