/*
 * @Date: 2022-06-11 21:25:06
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-11 21:28:48
 * @FilePath: \my-taro-app\src\store\hooks.ts
 * @Description: redux-hooks
 */
import {
  useSelector as useReduxSelector,
  TypedUseSelectorHook
} from "react-redux";
import { rootStateType } from "./index";
export const useSelector: TypedUseSelectorHook<rootStateType> = useReduxSelector;
