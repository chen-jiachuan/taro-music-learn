/*
 * @Date: 2022-06-12 12:34:16
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-22 09:11:25
 * @FilePath: \taro-music-learn\src\store\home\index.ts
 * @Description: something
 */
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { getBanner, reqRecommend } from "@/api/songs";
import { PromiseFormat } from "@/utils/request";
export interface HomeState {
  bannerList: any[];
  recommendList: any[];
}
export const fetchBannerList = createAsyncThunk(
  "HomeSlice/fetchBannerList",
  async (_, thunkAPI) => {
    const res = await getBanner();
    return res.data;
  }
);
export const fetchRecommendList = createAsyncThunk(
  "HomeSlice/fetchRecommendList",
  async (_, thunkAPI) => {
    const res = await reqRecommend();
    return PromiseFormat(res.data);
  }
);
const initialState: HomeState = {
  bannerList: [],
  recommendList: []
};

export const HomeSlice = createSlice({
  name: "HomeSlice",
  initialState,
  reducers: {},
  extraReducers: {
    [fetchBannerList.fulfilled.type]: (state, action) => {
      state.bannerList = action.payload.banners;
    },
    //获取热门推荐
    [fetchRecommendList.fulfilled.type]: (state, action) => {
      state.recommendList = action.payload.result;
    }
  }
});
