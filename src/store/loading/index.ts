/*
 * @Date: 2022-06-24 15:17:46
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-24 15:41:54
 * @FilePath: \taro-music-learn\src\store\loading\index.ts
 * @Description: 请求状态redux切片
 */
import { createSlice } from "@reduxjs/toolkit";
export interface ILoading {
  is_loading: boolean;
}
const initialState = {
  is_loading: false
};

export const LoadingSlice = createSlice({
  name: "LoadingSlice",
  initialState,
  reducers: {
    requestPending: state => {
      state.is_loading = true;
    },
    requestFulfilled: state => {
      state.is_loading = false;
    },
    requestRejectd: state => {
      state.is_loading = false;
    }
  }
});
export const {
  requestPending,
  requestFulfilled,
  requestRejectd
} = LoadingSlice.actions;
