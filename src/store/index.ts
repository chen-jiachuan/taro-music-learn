/*
 * @Date: 2022-06-11 21:25:15
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-11 21:28:10
 * @FilePath: \my-taro-app\src\store\index.ts
 * @Description: redux配置
 */
import { configureStore, getDefaultMiddleware } from "@reduxjs/toolkit";
import reducer from "./reducers";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
const storageConfig = {
  key: "root", // 必须有的
  storage, // 缓存机制
  whiteList: ["User"]
};
// 基于 localStorage 的 reducer
const persistedReducer = persistReducer(storageConfig, reducer);
const store = configureStore({
  reducer: persistedReducer,
  middleware: getDefaultMiddleware => [...getDefaultMiddleware()],
  devTools: true
});
// 基于 localStorage 的 store、
const persistor = persistStore(store);
export type rootStateType = ReturnType<typeof store.getState>;
export default { store, persistor };
