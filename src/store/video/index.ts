/*
 * @Date: 2022-06-30 14:31:08
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-07-04 15:38:55
 * @FilePath: \taro-music-learn\src\store\video\index.ts
 * @Description: something
 */
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import {
  reqVideoDetailInfo,
  reqVideoURLByVid,
  reqMvDetailInfo,
  reqMvUrlById
} from "@/api/video";
import { reqSingerDetailInfoById } from "@/api/user";
import { reqPersonalInfo } from "@/api/user";
import { PromiseFormat } from "@/utils/request";
type TCreator = {
  name: string;
  id: number;
  avatar: string;
  backgroundUrl: string;
};
export interface IInitialState {
  currrentVideoInfo: {
    commentCount: number;
    liked: boolean;
    likedCount: number;
    shareCount: number;
    url: string;
    title: string;
    creator: TCreator;
  };
}
const initialState: IInitialState = {
  currrentVideoInfo: {
    commentCount: 0,
    liked: false,
    likedCount: 0,
    shareCount: 0,
    url: "",
    title: "",
    creator: {
      name: "",
      id: -1,
      avatar: "",
      backgroundUrl: ""
    }
  }
};
export const fetchVideoDetailInfo = createAsyncThunk(
  "VideoSlice/fetchVedioDetailInfo",
  async (vid: string, thunkAPI) => {
    const { data } = await reqVideoDetailInfo(vid);
    return PromiseFormat(data);
  }
);
export const fetchMvDetailInfo = createAsyncThunk(
  "VideoSlice/fetchMvDetailInfo",
  async (vid: string, thunkAPI) => {
    const { data } = await reqMvDetailInfo(vid);
    return PromiseFormat(data);
  }
);
export const fetchVideoUrlByVid = createAsyncThunk(
  "VideoSlice/fetchVideoUrlByVid",
  async (vid: string, thunkAPI) => {
    const { data } = await reqVideoURLByVid(vid);
    return PromiseFormat(data);
  }
);
export const fetchCreatorInfoById = createAsyncThunk(
  "VideoSlice/fetchCreatorInfoById",
  async (uid: number | string, thunkAPI) => {
    try {
      const res = await reqPersonalInfo(uid);
      console.log("用户信息", res);
      if (res.data.code === 404) {
        throw Error;
      }
      return PromiseFormat(res.data);
    } catch (error) {
      const res = await reqSingerDetailInfoById(uid);
      console.log("歌手信息", res);
      return PromiseFormat(res.data);
    }
  }
);
export const fetchMvUrlById = createAsyncThunk(
  "VideoSlice/fetchMvUrlById",
  async (vid: string, thunkAPI) => {
    const { data } = await reqMvUrlById(vid);
    return PromiseFormat(data);
  }
);
export const VideoSlice = createSlice({
  name: "VideoSlice",
  initialState,
  reducers: {
    updateVideoTitle: (state, action) => {
      state.currrentVideoInfo.title = action.payload;
    },
    clearCurrentVedioInfo: (state, action) => {
      state.currrentVideoInfo.commentCount = 0;
      state.currrentVideoInfo.likedCount = 0;
      state.currrentVideoInfo.shareCount = 0;
    }
  },
  extraReducers: {
    [fetchVideoDetailInfo.fulfilled.type]: (state, action) => {
      console.log("视频信息", action.payload);
      const { commentCount, liked, likedCount, shareCount } = action.payload;
      state.currrentVideoInfo.commentCount = commentCount;
      state.currrentVideoInfo.liked = liked;
      state.currrentVideoInfo.likedCount = likedCount;
      state.currrentVideoInfo.shareCount = shareCount;
    },
    //video地址
    [fetchVideoUrlByVid.fulfilled.type]: (state, action) => {
      state.currrentVideoInfo.url = action.payload.urls[0].url;
    },
    //mv地址
    [fetchMvUrlById.fulfilled.type]: (state, action) => {
      state.currrentVideoInfo.url = action.payload.data.url;
    },
    //mv详情
    [fetchMvDetailInfo.fulfilled.type]: (state, action) => {
      console.log("mv信息", action.payload);
      const { commentCount, liked, likedCount, shareCount } = action.payload;
      state.currrentVideoInfo.commentCount = commentCount;
      state.currrentVideoInfo.liked = liked;
      state.currrentVideoInfo.likedCount = likedCount;
      state.currrentVideoInfo.shareCount = shareCount;
    },
    // 视频作者信息
    [fetchCreatorInfoById.fulfilled.type]: (state, action) => {
      console.log("作者信息是", action.payload);
      const { profile } = action.payload;
      if (profile) {
        state.currrentVideoInfo.creator.name = profile.nickname;
        state.currrentVideoInfo.creator.id = profile.userId;
        state.currrentVideoInfo.creator.avatar = profile.avatarUrl;
        state.currrentVideoInfo.creator.backgroundUrl = profile.backgroundUrl;
      } else {
        const { artist } = action.payload.data;
        state.currrentVideoInfo.creator.name = artist.name;
        state.currrentVideoInfo.creator.id = artist.id;
        state.currrentVideoInfo.creator.avatar = artist.cover;
        state.currrentVideoInfo.creator.backgroundUrl = artist.cover;
      }
    }
  }
});
export const { updateVideoTitle, clearCurrentVedioInfo } = VideoSlice.actions;
