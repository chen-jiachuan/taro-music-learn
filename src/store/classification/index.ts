/*
 * @Date: 2022-06-27 15:23:59
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-30 15:52:36
 * @FilePath: \taro-music-learn\src\store\classification\index.ts
 * @Description: something
 */
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { reqSingerList, reqSearchData } from "@/api/search";
import { PromiseFormat } from "@/utils/request";
type TSinger = {
  picUrl: string;
  name: string;
};
export interface IInitialStaet {
  singerList: TSinger[];
  searchSongs: { songs: any[]; songCount: number };
  searchAlnums: { albums: any[]; albumCount: number };
  searchPlayLists: { playlists: any[]; playlistCount: number };
  searchArtists: { artists: any[]; artistCount: number };
  searchVideos: { videos: any[]; videoCount: number };
}
const initialState: IInitialStaet = {
  singerList: [],
  searchSongs: {
    songs: [],
    songCount: 0
  },
  searchAlnums: {
    albums: [],
    albumCount: 0
  },
  searchPlayLists: {
    playlists: [],
    playlistCount: 0
  },
  searchArtists: {
    artists: [],
    artistCount: 0
  },
  searchVideos: {
    videos: [],
    videoCount: 0
  }
};
export const fetchSearchSongData = createAsyncThunk(
  "ClassificationSlice/fetchSearchSongData",
  async (
    parameters: {
      keywords: string;
      offset: number;
      type: number;
      limit?: number;
    },
    thunkAPI
  ) => {
    const { keywords, offset, type, limit } = parameters;
    const res = await reqSearchData(keywords, offset, type, limit);
    console.log("歌曲数据", res);
    return PromiseFormat(res.data);
  }
);
export const fetchSearchAlbumData = createAsyncThunk(
  "ClassificationSlice/fetchSearchAlbumData",
  async (
    parameters: {
      keywords: string;
      offset: number;
      type: number;
      limit?: number;
    },
    thunkAPI
  ) => {
    const { keywords, offset, type, limit } = parameters;
    const res = await reqSearchData(keywords, offset, type, limit);
    console.log("专辑数据", res);
    return PromiseFormat(res.data);
  }
);
export const fetchSearchPlayListData = createAsyncThunk(
  "ClassificationSlice/fetchSearchPlayListData",
  async (
    parameters: {
      keywords: string;
      offset: number;
      type: number;
      limit?: number;
    },
    thunkAPI
  ) => {
    const { keywords, offset, type, limit } = parameters;
    const res = await reqSearchData(keywords, offset, type, limit);
    console.log("歌单数据", res);
    return PromiseFormat(res.data);
  }
);
export const fetchSearchSingerData = createAsyncThunk(
  "ClassificationSlice/fetchSearchSingerData",
  async (
    parameters: {
      keywords: string;
      offset: number;
      type: number;
      limit?: number;
    },
    thunkAPI
  ) => {
    const { keywords, offset, type, limit } = parameters;
    const res = await reqSearchData(keywords, offset, type, limit);
    console.log("歌手数据", res);
    return PromiseFormat(res.data);
  }
);
export const fetchSearchVideoData = createAsyncThunk(
  "ClassificationSlice/fetchSearchVideoData",
  async (
    parameters: {
      keywords: string;
      offset: number;
      type: number;
      limit?: number;
    },
    thunkAPI
  ) => {
    const { keywords, offset, type, limit } = parameters;
    const res = await reqSearchData(keywords, offset, type, limit);
    console.log("视频数据", res);
    return PromiseFormat(res.data);
  }
);
export const fetchSingListByType = createAsyncThunk(
  "ClassificationSlice/reqSingerList",
  async (
    params: { type: number; area: number; initial: number; offset: number },
    thunkAPI
  ) => {
    let res = await reqSingerList(params);
    return PromiseFormat(res.data);
  }
);
export const ClassificationSlice = createSlice({
  name: "ClassificationSlice",
  initialState,
  reducers: {
    clearSingerList: state => {
      state.singerList = [];
    },
    clearSearchData: state => {
      state.searchAlnums.albumCount = 0;
      state.searchAlnums.albums = [];
      state.searchArtists.artistCount = 0;
      state.searchArtists.artists = [];
      state.searchPlayLists.playlistCount = 0;
      state.searchPlayLists.playlists = [];
      state.searchSongs.songCount = 0;
      state.searchSongs.songs = [];
      state.searchVideos.videos = [];
      state.searchVideos.videoCount = 0;
    }
  },
  extraReducers: {
    [fetchSingListByType.fulfilled.type]: (state, action) => {
      const arr = [...state.singerList, ...action.payload.artists];
      state.singerList = arr;
    },
    [fetchSearchSongData.fulfilled.type]: (state, action) => {
      const { songs, songCount } = action.payload.result;
      state.searchSongs.songCount = songCount;
      state.searchSongs.songs = [...state.searchSongs.songs, ...songs];
    },
    [fetchSearchAlbumData.fulfilled.type]: (state, action) => {
      const { albums, albumCount } = action.payload.result;
      state.searchAlnums.albumCount = albumCount;
      state.searchAlnums.albums = [...state.searchAlnums.albums, ...albums];
    },
    [fetchSearchPlayListData.fulfilled.type]: (state, action) => {
      const { playlists, playlistCount } = action.payload.result;
      state.searchPlayLists.playlistCount = playlistCount;
      state.searchPlayLists.playlists = [
        ...state.searchPlayLists.playlists,
        ...playlists
      ];
    },
    [fetchSearchSingerData.fulfilled.type]: (state, action) => {
      const { artists, artistCount } = action.payload.result;
      state.searchArtists.artistCount = artistCount;
      state.searchArtists.artists = [
        ...state.searchArtists.artists,
        ...artists
      ];
    },
    [fetchSearchVideoData.fulfilled.type]: (state, action) => {
      const { videos, videoCount } = action.payload.result;
      state.searchVideos.videos = [...state.searchVideos.videos, ...videos];
      state.searchVideos.videoCount = videoCount;
    }
  }
});
export const { clearSingerList, clearSearchData } = ClassificationSlice.actions;
