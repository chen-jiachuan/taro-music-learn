/*
 * @Date: 2022-06-15 16:42:30
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-15 20:04:02
 * @FilePath: \my-taro-app\src\store\personnal\index.ts
 * @Description: 私人信息
 */
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { PromiseFormat } from "@/utils/request";
import { reqPersonalInfo } from "@/api/user";
export interface PersonalState {
  personalInfo: { level: number; profile: any };
}
export const fetchPersonnalInfo = createAsyncThunk(
  "PersonalSlice/fetchPersonnalInfo",
  async (uid: number | string, thunkAPI) => {
    const res = await reqPersonalInfo(uid);
    return PromiseFormat(res.data);
  }
);
const initialState: PersonalState = {
  personalInfo: {
    level: 0,
    profile: {}
  }
};
export const PersonalSlice = createSlice({
  name: "PersonalSlice",
  initialState,
  reducers: {},
  extraReducers: {
    [fetchPersonnalInfo.fulfilled.type]: (state, action) => {
      state.personalInfo.level = action.payload.level;
      state.personalInfo.profile = action.payload.profile;
    }
  }
});
