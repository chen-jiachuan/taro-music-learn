/*
 * @Date: 2022-06-11 21:25:02
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-30 14:35:46
 * @FilePath: \taro-music-learn\src\store\reducers.ts
 * @Description: reducers
 */
import { combineReducers } from "@reduxjs/toolkit";
import { UserSlice } from "./user";
import { HomeSlice } from "./home";
import { PersonalSlice } from "./personnal";
import { PlaySongSlice } from "./playSong";
import { LoadingSlice } from "./loading";
import { ClassificationSlice } from "./classification";
import { VideoSlice } from "./video";
const reducers = combineReducers({
  User: UserSlice.reducer,
  Home: HomeSlice.reducer,
  Personal: PersonalSlice.reducer,
  PlaySong: PlaySongSlice.reducer,
  Loading: LoadingSlice.reducer,
  Classification: ClassificationSlice.reducer,
  Video: VideoSlice.reducer
});
export default reducers;
