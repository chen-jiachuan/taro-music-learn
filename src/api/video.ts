/*
 * @Date: 2022-06-30 14:27:20
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-07-06 15:19:16
 * @FilePath: \taro-music-learn\src\api\video.ts
 * @Description: 视频api
 */
import { request1 } from "@/utils/request";
//获取视频详情
export const reqVideoDetailInfo = (vid: string) => {
  return request1("/video/detail/info", "GET", { vid });
};

//获取视频播放地址
export const reqVideoURLByVid = (id: string) => {
  return request1("/video/url", "GET", { id });
};

//获取mv详情
export const reqMvDetailInfo = (mvid: string) => {
  return request1("/mv/detail/info", "GET", { mvid });
};

//获取mv播放地址
export const reqMvUrlById = (id: string) => {
  return request1("/mv/url", "GET", { id });
};

//资源点赞
/**
 * @param id 资源对应id
 * @param type 1: mv 4: 电台 5: 视频 6: 动态
 * @param t 操作,1 为点赞,其他为取消点赞
 * @returns void
 */
export const reqLikeByType = (
  id: string | number,
  type: 1 | 4 | 5 | 6,
  t: boolean | number
) => {
  t ? (t = 1) : (t = 0);
  return request1("/resource/like", "GET", { id, type, t });
};

//收藏mv
/**
 * @param mvid mv id
 * @param t 1 为收藏,其他为取消收藏
 * @returns void
 */
export const reqSubMvById = (
  mvid: string | number,
  t: true | false | number
) => {
  if (t) {
    t = 1;
  } else {
    t = 0;
  }
  return request1("/mv/sub", "GET", { mvid, t });
};
//收藏视频
/**
 * @param id 视频 id
 * @param t 1 为收藏,其他为取消收藏
 * @returns void
 */
export const reqSubVideoById = (
  id: string | number,
  t: true | false | number
) => {
  if (t) {
    t = 1;
  } else {
    t = 0;
  }
  return request1("/video/sub", "GET", { id, t });
};

//获取mv评论(最新)
export const reqMvCommentById = (id: number | string, offset: number) => {
  return request1("/comment/mv", "GET", { id, offset });
};
//获取视频评论(最新)
export const reqVedioCommentById = (id: string, offset: number) => {
  return request1("/comment/video", "GET", { id, offset });
};
/**
 *
 * @param id 资源id
 * @param type 0: 歌曲 1: mv 2: 歌单 3: 专辑 4: 电台 5: 视频 6: 动态
 * @param pageNo 分页数，第几页
 * @param pageSize 分页参数,每页多少条数据,默认 20
 * @param sortType  排序方式, 1:按推荐排序, 2:按热度排序, 3:按时间排序
 * @returns
 */
//获取热门评论
export const reqHotCommentByType = (
  id: number | string,
  type: number,
  pageNo: number,
  pageSize: number = 20,
  sortType: 1 | 2 | 3 = 2
) => {
  // offset = (offset - 1) * 20;
  return request1("/comment/new", "GET", {
    id,
    type,
    pageNo,
    pageSize,
    sortType
  });
};
/**
 *
 * @param t 1 发送, 2 回复
 * @param type 0: 歌曲 1: mv 2: 歌单 3: 专辑 4: 电台 5: 视频 6: 动态
 * @param id 对应资源 id
 * @param content 要发送的内容
 * @param commentId 回复的评论 id (回复评论时必填)
 * @returns RequestTask<any>
 */
//创建评论或者回复
export const reqCreatCommentOrReply = (
  t: 1 | 2,
  type: number,
  id: number | string,
  content: string,
  commentId?: number
) => {
  let data;
  if (commentId) {
    data = { t, type, id, content, commentId };
  } else {
    data = { t, type, id, content };
  }
  return request1("/comment", "GET", data);
};
/**
 *
 * @param parentCommentId 父级评论Id
 * @param id 资源id
 * @param type 0:歌曲 1:mv 2:歌单 3:专辑 4:电台 5:视频
 * @returns
 */
//获取评论回复
export const reqCommentReplyByParentId = (
  parentCommentId: number | string,
  id: number | string,
  type: number | string
) => {
  return request1("/comment/floor", "GET", { parentCommentId, id, type });
};
