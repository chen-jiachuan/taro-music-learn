/*
 * @Date: 2022-06-12 10:22:56
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-07-01 15:33:48
 * @FilePath: \taro-music-learn\src\api\songs.ts
 * @Description: 歌曲相关api
 */
import { request1 } from "@/utils/request";
export const getBanner = () => {
  return request1("/banner", "GET");
};
//获取用户的歌单
export const reqUserMusicList = (uid: number | string) => {
  return request1("/user/playlist", "GET", {
    uid
  });
};

//获取热门推荐歌单
export const reqRecommend = () => {
  return request1("/personalized", "GET");
};

//获取歌曲详情
export const reqSongDetailAction = (ids: string | number) => {
  return request1("/song/detail", "GET", { ids });
};
//获取歌词
export const reqLyric = (id: string | number) => {
  return request1("/lyric", "GET", { id });
};

//获取歌单详情
export const reqSingListDetail = (id: number | string) => {
  return request1("/playlist/detail", "GET", {
    id
  });
};
//喜欢歌曲
export const reqLikeMusicById = (id: number, like: boolean = true) => {
  return request1("/like", "GET", { id, like });
};

//获取推荐新音乐
export const reqSuggessMusic = () => {
  return request1("/personalized/newsong", "GET");
};

//获取歌单评论
export const reqPlayListCommentById = (id: number, offset: number) => {
  return request1("/comment/playlist", "GET", { id, offset });
};
