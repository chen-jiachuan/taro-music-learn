/*
 * @Date: 2022-06-27 09:46:00
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-30 09:16:06
 * @FilePath: \taro-music-learn\src\api\search.ts
 * @Description: 话题接口
 */
import { request1 } from "@/utils/request";

//获取热门话题
export const reqHotTopicList = () => {
  return request1("/hot/topic", "GET");
};

//获取热门搜索
export const reqHotSearchList = () => {
  return request1("/search/hot/detail", "GET");
};

//获取热门歌手
export const reqHotSingerList = () => {
  return request1("/top/artists?offset=0&limit=20", "GET");
};

//获取歌手分类列表 type: -1:全部 1:男歌手 2女歌手 3乐队 area -1全部 7华语 96欧美 8日本 16韩国 0其他
export const reqSingerList = (params: {
  type: number;
  area: number;
  initial: any;
  offset: number;
}) => {
  const { type, area, initial, offset = 30 } = params;
  return request1("/artist/list", "GET", { type, area, initial, offset });
};

//搜索建议
export const reqSearchSuggest = (keywords: string) => {
  return request1("/search/suggest", "GET", { keywords });
};

//根据关键词搜索结果 搜索类型；
/**
 *  @keyword 必选
 *  @offset 偏移数量 默认为 30 用于分页 , 如 : 如 :( 页数 -1)*30, 其中 30 为 limit 的值 , 默认为 0
 *  @type 默认为 1 即单曲 , type取值意义 : 1: 单曲, 10: 专辑, 100: 歌手, 1000: 歌单, 1002: 用户, 1004: MV, 1006: 歌词, 1009: 电台, 1014: 视频, 1018:综合, 2000:声音(搜索声音返回字段格式会不一样)
 */
export const reqSearchData = (
  keywords: string,
  offset: number,
  type?: number,
  limit: number = 30
) => {
  offset = limit * (offset - 1);
  return request1("/cloudsearch", "GET", { keywords, offset, type, limit });
};
