/*
 * @Date: 2022-06-12 12:58:09
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-07-04 17:20:54
 * @FilePath: \taro-music-learn\src\api\user.ts
 * @Description: 用户api
 */
import { request1 } from "@/utils/request";
export const getUserInfo = () => {
  // let timeStamp = Date.now();
  return request1("/user/account", "GET");
};
export const reqLogin = (phone: string, password: string) => {
  //由于缓存机制，文档显示必须加上当前时间戳
  return request1("/login/cellphone", "GET", {
    phone,
    password
  });
};
//退出登录
export const reqLogout = () => {
  return request1("/logout", "GET");
};
// 获取私人信息
export const reqPersonalInfo = (uid: number | string) => {
  return request1("/user/detail", "GET", { uid });
};

//获取喜欢的音乐列表
export const reqUserLikeMusicList = uid => {
  return request1("/likelist", "GET", { uid });
};

//获取关注的歌手列表
export const reqUserSubSingerList = () => {
  return request1("/artist/sublist", "GET", { limit: 1000 });
};

//获取歌手信息
export const reqSingerDetailInfoById = (id: number | string) => {
  return request1("/artist/detail", "GET", { id });
};

//获取收藏的视频列表
export const reqSubVideoList = () => {
  return request1(`/mv/sublist`, "GET", { limit: 1000 });
};

//获取点赞过的视频列表
export const reqLikeVideoList = () => {
  return request1("/playlist/mylike", "GET");
};
