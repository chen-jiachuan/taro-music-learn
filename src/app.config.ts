/*
 * @Date: 2022-06-11 21:14:48
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-07-05 16:55:08
 * @FilePath: \taro-music-learn\src\app.config.ts
 * @Description:
 */
export default {
  pages: ["pages/index/index", "pages/login/index", "pages/my/index"],
  window: {
    backgroundTextStyle: "light",
    navigationBarBackgroundColor: "#ff0000",
    navigationBarTitleText: "WeChat",
    navigationBarTextStyle: "white"
  },
  requiredBackgroundModes: ["audio"],
  enablePullDownRefresh: true,
  onReachBottomDistance: 200,
  subpackages: [
    {
      root: "pages/packageA/",
      pages: [
        "userDetail/index",
        "songDetail/index",
        "MusicListDetail/index",
        "search/index",
        "singerList/index",
        "searchDetailInfo/index",
        "VideoDetail/index",
        "CommentDetail/index"
      ]
    }
  ],
  tabBar: {
    color: "#000",
    selectedColor: "#ff0000",
    backgroundColor: "white",
    borderStyle: "white",
    fontSize: "16px",
    list: [
      {
        text: "首页",
        pagePath: "pages/index/index",
        iconPath: "./assets/icons/home.png", // 激活前的图片
        selectedIconPath: "./assets/icons/homeactive.png" // 激活后的图片
      },
      {
        text: "我的",
        pagePath: "pages/my/index",
        iconPath: "./assets/icons/my.png", // 激活前的图片
        selectedIconPath: "./assets/icons/my-active.png" // 激活后的图片
      }
    ]
  }
};
