/*
 * @Date: 2022-06-15 16:12:28
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-29 14:26:54
 * @FilePath: \taro-music-learn\src\utils\index.ts
 * @Description: tool handler
 */
//播放量格式化
export const playCountFormat = (playCount: number) => {
  if (playCount > 10000) {
    playCount = +(playCount / 10000).toFixed(1);
    return playCount + "万";
  } else {
    return playCount;
  }
};

//歌词解析
const parseExp = /\[(\d{2}):(\d{2})\.(\d{2,3})\]/;
export const parseLyric = (lyricObj: string) => {
  const lineStrings = lyricObj.split("\n");
  const lyrics: Array<{ t: number; content: string }> = [];
  for (const lineStrs of lineStrings) {
    if (lineStrs) {
      const res = parseExp.exec(lineStrs) as any;
      const t1 = res[1] * 60 * 1000;
      const t2 = res[2] * 1000;
      const t3 = res[3].length === 3 ? res[3] * 1 : res[3] * 10;
      const t = (t1 + t2 + t3) / 1000;
      const content = lineStrs.replace(parseExp, "").trim();
      const obj = { t, content };
      if (obj.content !== "") {
        lyrics.push(obj);
      }
    }
  }
  return lyrics;
};
//获取与上次不重复随机数
export const getRandomIntInclusive = (
  min: number,
  max: number,
  prevNum: number
) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  let randomNum = Math.floor(Math.random() * (max - min + 1)) + min;
  while (prevNum === randomNum) {
    randomNum = Math.floor(Math.random() * (max - min + 1)) + min;
  }
  return randomNum; //含最大值，含最小值
};
//时长处理
export const TimeHandler = (time: number) => {
  if (time > 10000) {
    time = Math.floor(time / 1000);
  } else {
    time = Math.floor(time);
  }
  let s: number | string = Math.floor(time % 60);
  let m: number | string = Math.floor(time / 60);
  s >= 10 ? (s = s) : (s = `0${s}`);
  m >= 10 ? (m = m) : (m = `0${m}`);
  return m + ":" + s;
};
