/*
 * @Date: 2022-06-11 21:14:48
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-27 17:47:32
 * @FilePath: \taro-music-learn\src\pages\index\index.tsx
 * @Description: something
 */
import React, { useEffect, useRef, useState } from "react";
import { View, Text, Swiper, SwiperItem, Image } from "@tarojs/components";
import ImgListShow from "@/components/ImgListShow";
import {
  AtButton,
  AtSearchBar,
  AtDrawer,
  AtIcon,
  AtAvatar,
  AtAccordion,
  AtList,
  AtListItem
} from "taro-ui";
import { fetchBannerList, fetchRecommendList } from "@/store/home";
import { fetchUserInfo, fetchLogout } from "@/store/user";
import { useDispatch } from "react-redux";
import { useSelector } from "@/store/hooks";
import Taro from "@tarojs/taro";
import AudioCmp from "@/components/AudioCmp";
import LoadingCmp from "@/components/LoadingCmp";
import "./index.scss";
import { fetchSongDetailInfo } from "@/store/playSong";
import MusicListItem from "@/components/MusicListItem";
const Index: React.FC = () => {
  const [drawerShowFlag, setDrawerShowFlag] = useState<boolean>(false);
  const [collectionShowFlag, setCollectionShowFlag] = useState(false);
  const [ownShowFlag, setOwnShowFlag] = useState(false);
  const [isFocus, setIsFocus] = useState(false);
  const dispatch = useDispatch();
  const { HomeState, UserState } = useSelector(state => ({
    HomeState: state.Home,
    UserState: state.User,
    currentSongState: state.PlaySong
  }));
  useEffect(() => {
    fetchAction();
  }, [UserState.isLogin]);
  const fetchAction = async () => {
    dispatch<any>(fetchUserInfo());
    dispatch<any>(fetchBannerList());
    dispatch<any>(fetchRecommendList());
  };
  const toggleDrawer = () => {
    setDrawerShowFlag(!drawerShowFlag);
  };
  const go2Login = () => {
    Taro.navigateTo({
      url: "/pages/login/index"
    });
  };
  const go2Search = () => {
    Taro.navigateTo({
      url: "/pages/packageA/search/index"
    });
  };
  const logOut = () => {
    dispatch<any>(fetchLogout());
  };
  const collectionHandleClick = () => {
    setCollectionShowFlag(!collectionShowFlag);
  };
  const ownHandleClick = () => {
    setOwnShowFlag(!ownShowFlag);
  };
  const go2MusicListDetail = (id: number) => {
    console.log("跳转id", id);
    Taro.navigateTo({
      url: "/pages/packageA/MusicListDetail/index?id=" + id
    });
  };
  return (
    <View id="home-container">
      <LoadingCmp />
      <View className="home-header">
        <AtIcon value="list" onClick={toggleDrawer}></AtIcon>
        <AtSearchBar
          className="home-header-searchbar"
          onFocus={go2Search}
          focus={isFocus}
        ></AtSearchBar>
      </View>
      <Swiper
        className="banner-list"
        indicatorColor="#999"
        indicatorActiveColor="#fff"
        circular
        indicatorDots
        autoplay
      >
        {HomeState.bannerList.map(item => {
          return (
            <SwiperItem key={item.imageUrl} className="banner-item">
              <Image className="banner-item-img" src={item.imageUrl}></Image>
            </SwiperItem>
          );
        })}
      </Swiper>
      <View className="handle_list">
        <View
          className="handle_list__item"
          onClick={() => {
            dispatch<any>(fetchSongDetailInfo(1954420092));
            // dispatch<any>(fetchSongDetailInfo(1922888354));
            // dispatch<any>(fetchSongDetailInfo(1956525809));
          }}
        >
          <View className="handle_list__item__icon-wrap">
            <AtIcon
              value="calendar"
              size="25"
              color="#ffffff"
              className="handle_list_item__icon"
            ></AtIcon>
          </View>
          <Text className="handle_list__item__text">每日推荐</Text>
        </View>
      </View>
      {/* 侧面抽屉个人信息 */}
      <ImgListShow
        title="推荐歌单"
        imgList={HomeState.recommendList}
      ></ImgListShow>
      <AtDrawer
        show={drawerShowFlag}
        mask
        onClose={toggleDrawer}
        className="drawer"
      >
        <View className="drawer-item user-box">
          <View>
            <AtAvatar
              circle
              image={
                UserState.isLogin && UserState.userInfo.avatarUrl
                  ? UserState.userInfo.avatarUrl
                  : "https://jdc.jd.com/img/200"
              }
            ></AtAvatar>
          </View>
          <Text style={"font-size:0.75rem;padding:0 0.625rem"}>
            {UserState.userInfo.nickname ? UserState.userInfo.nickname : true}
          </Text>
          {!UserState.isLogin ? (
            <AtButton type="primary" size="small" onClick={go2Login}>
              登录
            </AtButton>
          ) : (
            <AtButton type="primary" size="small" onClick={logOut}>
              退出登录
            </AtButton>
          )}
        </View>
        <View className="drawer-item">
          <AtAccordion
            open={collectionShowFlag}
            onClick={collectionHandleClick}
            title="我收藏的歌单"
          >
            <AtList hasBorder={false}>
              {UserState.collectionMusicList.map(item => {
                return (
                  <View
                    onClick={() => {
                      go2MusicListDetail(item.id);
                    }}
                  >
                    <MusicListItem musicInfo={item}></MusicListItem>
                  </View>
                );
              })}
            </AtList>
          </AtAccordion>
        </View>
        <View className="drawer-item">
          <AtAccordion
            open={ownShowFlag}
            onClick={ownHandleClick}
            title="我创建的歌单"
          >
            <AtList hasBorder={false}>
              {UserState.ownMusciList.map(item => {
                return (
                  <View
                    onClick={() => {
                      go2MusicListDetail(item.id);
                    }}
                  >
                    <MusicListItem musicInfo={item}></MusicListItem>
                  </View>
                );
              })}
            </AtList>
          </AtAccordion>
        </View>
      </AtDrawer>
      <AudioCmp />
    </View>
  );
};
export default Index;
