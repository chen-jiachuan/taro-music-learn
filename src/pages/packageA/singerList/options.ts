/*
 * @Date: 2022-06-27 15:47:18
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-27 16:49:57
 * @FilePath: \taro-music-learn\src\pages\packageA\singerList\options.ts
 * @Description: something
 */
export const languageList = [
  { area: -1, name: "全部", index: 0 },
  { area: 7, name: "华语", index: 1 },
  { area: 96, name: "欧美", index: 2 },
  { area: 8, name: "日本", index: 3 },
  { area: 16, name: "韩国", index: 4 },
  { area: 0, name: "其它", index: 5 }
];
//类型分类
export const typeList = [
  { type: 1, name: "男歌手", index: 0 },
  { type: 2, name: "女歌手", index: 1 },
  { type: 3, name: "乐队/组合", index: 2 }
];
