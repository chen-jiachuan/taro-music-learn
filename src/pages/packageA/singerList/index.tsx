/*
 * @Date: 2022-06-27 15:09:31
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-29 15:48:07
 * @FilePath: \taro-music-learn\src\pages\packageA\singerList\index.tsx
 * @Description: singerList
 */
import { View } from "@tarojs/components";
import { languageList, typeList } from "./options";
import React, { useEffect, useRef, useState } from "react";
import { AtActivityIndicator, AtAvatar, AtButton, AtTag } from "taro-ui";
import { useDispatch } from "react-redux";
import { useDidShow, useReachBottom } from "@tarojs/taro";
import { fetchSingListByType, clearSingerList } from "@/store/classification";
import { useSelector } from "@/store/hooks";
import GoTop from "@/components/GoTop";
import SingerItem from "@/components/SingerItem";
import "./index.scss";
export default function index() {
  const dispatch = useDispatch();
  const [langIdx, setLangIdx] = useState(-1);
  const [genderIdx, setGenderIdx] = useState(-1);
  const [isOpened, setIsOpened] = useState(false);
  const isMore = useRef<boolean>(false);
  const currentOffset = useRef(1);
  const chooses = useRef({
    initial: -1,
    type: -1,
    area: -1,
    offset: 0
  });
  const { Classification } = useSelector(state => ({
    Classification: state.Classification
  }));
  useDidShow(async () => {
    let res = await dispatch<any>(fetchSingListByType(chooses.current));
    if (res.meta.requestStatus === "fulfilled" && res.payload.more) {
      isMore.current = true;
    }
  });
  //触底加载
  useReachBottom(async () => {
    console.log("触底了", isMore.current);
    if (isMore.current) {
      setIsOpened(true);
      currentOffset.current = currentOffset.current + 1;
      chooses.current.offset = 30 * (currentOffset.current - 1);
      console.log(chooses.current);
      let res = await dispatch<any>(fetchSingListByType(chooses.current));
      if (res.meta.requestStatus === "fulfilled") {
        setIsOpened(false);
        if (res.payload.more) {
          isMore.current = true;
        } else {
          isMore.current = false;
        }
      } else {
        setIsOpened(false);
      }
    } else {
      return;
    }
  });
  const choose = async (tagInfo, type) => {
    let arr;
    if (type === "lang") {
      arr = [...languageList];
    } else {
      arr = [...typeList];
    }
    const idx = arr.findIndex(item => {
      return tagInfo.name === item.name;
    });
    const chooseObj = arr.find(item => {
      return tagInfo.name === item.name;
    });
    if (type === "lang") {
      await setLangIdx(prev => {
        if (prev === idx) {
          chooses.current.area = -1;
          return -1;
        } else {
          chooses.current.area = chooseObj.area;
          return idx;
        }
      });
    } else {
      await setGenderIdx(prev => {
        if (prev === idx) {
          chooses.current.type = -1;
          return -1;
        } else {
          chooses.current.type = chooseObj.type;
          return idx;
        }
      });
    }
    chooses.current.offset = 1;
    dispatch(clearSingerList());
    const res = await dispatch<any>(fetchSingListByType(chooses.current));
    if (res.meta.requestStatus === "fulfilled") {
      if (res.payload.more) {
        isMore.current = true;
      } else {
        isMore.current = false;
      }
    }
  };
  return (
    <View id="singer-list-container-unique">
      <GoTop></GoTop>
      <View className="classification-header">
        <View className="lang-classification">
          {languageList.map((item, index) => {
            return (
              <AtTag
                key={index}
                active={langIdx === index}
                name={item.name}
                circle
                onClick={tagInfo => {
                  choose(tagInfo, "lang");
                }}
              >
                {item.name}
              </AtTag>
            );
          })}
        </View>
        <View className="gender-classification">
          {typeList.map((item, index) => {
            return (
              <AtTag
                key={index}
                active={genderIdx === index}
                name={item.name}
                circle
                onClick={tagInfo => {
                  choose(tagInfo, "gender");
                }}
              >
                {item.name}
              </AtTag>
            );
          })}
        </View>
      </View>
      <View>热门歌手</View>
      <View className="sing-list-container">
        {Classification.singerList.map((item, index) => {
          return (
            <View key={index} className="sing-list-container">
              <SingerItem singerInfo={item}></SingerItem>
            </View>
          );
        })}
        <AtActivityIndicator
          content="玩命加载中..."
          isOpened={isOpened}
          color="#ff0000"
        ></AtActivityIndicator>
      </View>
    </View>
  );
}
