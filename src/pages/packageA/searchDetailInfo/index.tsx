/*
 * @Date: 2022-06-28 15:27:30
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-30 16:42:19
 * @FilePath: \taro-music-learn\src\pages\packageA\searchDetailInfo\index.tsx
 * @Description: searchDetailInfo
 */
import { View } from "@tarojs/components";
import { useDidShow, useReachBottom, useRouter } from "@tarojs/taro";
import AudioCmp from "@/components/AudioCmp";
import Comprehensive from "@/components/Comprehensive";
import { useDispatch } from "react-redux";
import LoadingCmp from "@/components/LoadingCmp";
import "./index.scss";
import Taro from "@tarojs/taro";
import { AtTabs, AtTabsPane } from "taro-ui";
import { useContext, useEffect, useRef, useState } from "react";
import { useSelector } from "@/store/hooks";
import MusicInfoItem from "@/components/MusicInfoItem";
import MusicListItem from "@/components/MusicListItem";
import AlbumItem from "@/components/AlbumItem";
import VideoItem from "@/components/VedioItem";
import SingerItem from "@/components/SingerItem";
import Gotop from "@/components/GoTop";
import {
  fetchSearchSongData,
  fetchSearchAlbumData,
  fetchSearchPlayListData,
  fetchSearchSingerData,
  fetchSearchVideoData,
  clearSearchData
} from "@/store/classification";

export default function index() {
  const router = useRouter();
  const dispatch = useDispatch();
  const [currentTab, setCurrentTab] = useState(0);
  const songsCurOffset = useRef(1);
  const albumCurOffset = useRef(1);
  const playlistCurOffset = useRef(1);
  const singerCurOffset = useRef(1);
  const videoCurOffset = useRef(1);
  const {
    searchSongs,
    searchPlaylist,
    searchAlbums,
    searchVideos,
    searchSingers
  } = useSelector(state => ({
    searchSongs: state.Classification.searchSongs,
    searchPlaylist: state.Classification.searchPlayLists,
    searchAlbums: state.Classification.searchAlnums,
    searchVideos: state.Classification.searchVideos,
    searchSingers: state.Classification.searchArtists
  }));
  useEffect(() => {
    Taro.setNavigationBarTitle({
      title: `搜索到${router.params.keyword!}的资源`
    });
    Taro.eventCenter.on("clickBottom", (data: string) => {
      Taro.pageScrollTo({
        scrollTop: 0,
        duration: 0
      });
      getMoreInfo(data);
    });
    return () => {
      Taro.eventCenter.off("clickBottom");
    };
  }, []);
  useEffect(() => {
    Taro.pageScrollTo({
      scrollTop: 0,
      duration: 0
    });
  }, [currentTab]);
  const getMoreInfo = (data: string) => {
    if (data === "单曲") {
      setCurrentTab(1);
    } else if (data === "歌手") {
      setCurrentTab(4);
    } else if (data === "歌单") {
      setCurrentTab(3);
    } else if (data === "专辑") {
      setCurrentTab(2);
    } else if (data === "视频") {
      setCurrentTab(5);
    }
  };
  const changeTab = (value: number) => {
    setCurrentTab(value);
  };
  useReachBottom(() => {
    console.log("触底");
    const { keyword } = router.params;
    if (currentTab === 0) {
      return;
    } else if (currentTab === 1) {
      //单曲触底
      if (searchSongs.songs.length >= searchSongs.songCount) {
        return;
      }
      songsCurOffset.current = songsCurOffset.current + 1;
      dispatch<any>(
        fetchSearchSongData({
          keywords: keyword!,
          offset: songsCurOffset.current,
          type: 1
        })
      );
    } else if (currentTab === 2) {
      //专辑触底
      if (searchAlbums.albums.length >= searchAlbums.albumCount) {
        return;
      }
      albumCurOffset.current = albumCurOffset.current + 1;
      dispatch<any>(
        fetchSearchAlbumData({
          keywords: keyword!,
          offset: albumCurOffset.current,
          type: 10
        })
      );
    } else if (currentTab === 3) {
      //歌单触底
      if (searchPlaylist.playlists.length >= searchPlaylist.playlistCount) {
        return;
      }
      playlistCurOffset.current = playlistCurOffset.current + 1;
      dispatch<any>(
        fetchSearchPlayListData({
          keywords: keyword!,
          offset: playlistCurOffset.current,
          type: 1000
        })
      );
    } else if (currentTab === 4) {
      if (searchSingers.artists.length >= searchSingers.artistCount) {
        return;
      }
      //歌手触底
      singerCurOffset.current = singerCurOffset.current + 1;
      dispatch<any>(
        fetchSearchSingerData({
          keywords: keyword!,
          offset: singerCurOffset.current,
          type: 1000
        })
      );
    } else if (currentTab === 5) {
      //视频触底
      if (searchVideos.videos.length >= searchVideos.videoCount) {
        return;
      }
      videoCurOffset.current = videoCurOffset.current + 1;
      dispatch<any>(
        fetchSearchVideoData({
          keywords: keyword!,
          offset: videoCurOffset.current,
          type: 1014
        })
      );
    }
  });
  return (
    <View id="search-detail-info-container-unique">
      <LoadingCmp />
      <Gotop />
      <AtTabs
        current={currentTab}
        style={{ borderRadius: "0.625rem", height: "100vh" }}
        scroll
        tabList={[
          { title: "综合" },
          { title: "单曲" },
          { title: "专辑" },
          { title: "歌单" },
          { title: "歌手" },
          { title: "视频" }
        ]}
        onClick={value => {
          changeTab(value);
        }}
      >
        <AtTabsPane current={currentTab} index={0}>
          <View>
            <Comprehensive keyword={router.params.keyword!}></Comprehensive>
          </View>
        </AtTabsPane>
        <AtTabsPane current={currentTab} index={1}>
          <View className="tabpane-item">
            {searchSongs.songs.map((item, index) => {
              return (
                <View key={item.id}>
                  <MusicInfoItem
                    songInfo={item}
                    index={index + 1}
                  ></MusicInfoItem>
                </View>
              );
            })}
          </View>
        </AtTabsPane>
        <AtTabsPane current={currentTab} index={2}>
          <View className="tabpane-item">
            {searchAlbums.albums.map(item => {
              return (
                <View key={item.id} style={{ padding: "0.625rem 0" }}>
                  <AlbumItem albumInfo={item}></AlbumItem>
                </View>
              );
            })}
          </View>
        </AtTabsPane>
        <AtTabsPane current={currentTab} index={3}>
          <View className="tabpane-item">
            {searchPlaylist.playlists.map(item => {
              return (
                <View
                  key={item.id}
                  style={{
                    padding: "0.625rem 0"
                  }}
                >
                  <MusicListItem musicInfo={item}></MusicListItem>
                </View>
              );
            })}
          </View>
        </AtTabsPane>
        <AtTabsPane current={currentTab} index={4}>
          <View className="tabpane-item">
            {searchSingers.artists.map(item => {
              return (
                <View
                  key={item.id}
                  style={{
                    padding: "0.625rem 0"
                  }}
                >
                  <SingerItem singerInfo={item}></SingerItem>
                </View>
              );
            })}
          </View>
        </AtTabsPane>
        <AtTabsPane current={currentTab} index={5}>
          <View className="tabpane-item">
            {searchVideos.videos.map(item => {
              return (
                <View
                  key={item.vid}
                  style={{
                    padding: "0.625rem 0"
                  }}
                >
                  <VideoItem videoInfo={item}></VideoItem>
                </View>
              );
            })}
          </View>
        </AtTabsPane>
      </AtTabs>
      <AudioCmp></AudioCmp>
    </View>
  );
}
