/*
 * @Date: 2022-06-12 20:35:37
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-21 10:18:14
 * @FilePath: \taro-music-learn\src\pages\packageA\userDetail\index.tsx
 * @Description: 个人主页
 */
import React, { useEffect, memo, useState } from "react";
import Taro, { useDidShow, useRouter } from "@tarojs/taro";
import { View, Text } from "@tarojs/components";
import { useSelector } from "@/store/hooks";
import { AtAvatar, AtTabs, AtTabsPane, AtIcon } from "taro-ui";
import { fetchPersonnalInfo } from "@/store/personnal";
import { useDispatch } from "react-redux";
import { reqUserMusicList } from "@/api/songs";
import MusicListItem from "@/components/MusicListItem";
import "./index.scss";
import LoadingCmp from "@/components/LoadingCmp";

const index = () => {
  const tabList = [{ title: "创建歌单" }, { title: "收藏歌单" }];
  const router = useRouter();
  const dispatch = useDispatch();
  const [createMusicList, setCreateMusicList] = useState<Array<any>>([]);
  const [collectMusicList, setCollectMusicList] = useState<Array<any>>([]);
  const [currentTab, setCurrentTab] = useState<number>(0);
  const { PersonalState, UserState } = useSelector(state => ({
    PersonalState: state.Personal,
    UserState: state.User
  }));
  useDidShow(() => {
    if (router.params.userId) {
      dispatch<any>(fetchPersonnalInfo(router.params.userId!));
      fetchUserMusicList(router.params.userId!);
    } else {
      if (UserState.isLogin) {
        dispatch<any>(fetchPersonnalInfo(UserState.userInfo.userId));
        fetchUserMusicList(UserState.userInfo.userId);
      } else {
        Taro.navigateTo({
          url: "/pages/login/index"
        });
      }
    }
  });

  const { profile, level } = PersonalState.personalInfo;
  const fetchUserMusicList = async (uid: string) => {
    const res = await reqUserMusicList(uid);
    if (res.data.code === 200) {
      const creMLArr = res.data.playlist.filter((item: any) => {
        return item.subscribed === false;
      });
      const colMLArr = res.data.playlist.filter((item: any) => {
        return item.subscribed === true;
      });
      setCreateMusicList(creMLArr);
      setCollectMusicList(colMLArr);
    }
  };
  const clickTabsPane = (value: number) => {
    setCurrentTab(value);
  };
  return (
    <View id="user-detail-container-unique">
      <LoadingCmp />
      <View className="user-detail-header">
        <AtAvatar
          circle
          image={profile.avatarUrl}
          className="personal-avatar"
        ></AtAvatar>
        <Text className="nickname">{profile.nickname}</Text>
        <View className="user-other-info">
          <Text> {profile.follows} 关注</Text>
          <Text className="magin-class"> {profile.followeds} 粉丝</Text>
          <Text> Lv.{level} </Text>
        </View>
      </View>
      <View className="user-detail-container">
        <AtTabs current={currentTab} tabList={tabList} onClick={clickTabsPane}>
          <AtTabsPane current={currentTab} index={0}>
            <View className="create-tabspane">
              {createMusicList.map(item => {
                return <MusicListItem musicInfo={item}></MusicListItem>;
              })}
            </View>
          </AtTabsPane>
          <AtTabsPane current={currentTab} index={1}>
            <View className="create-tabspane">
              {collectMusicList.map(item => {
                return <MusicListItem musicInfo={item}></MusicListItem>;
              })}
            </View>
          </AtTabsPane>
        </AtTabs>
      </View>
    </View>
  );
};
export default index;
