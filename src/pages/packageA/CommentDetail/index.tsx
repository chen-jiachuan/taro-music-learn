/*
 * @Date: 2022-07-05 16:53:23
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-07-06 15:40:07
 * @FilePath: \taro-music-learn\src\pages\packageA\CommentDetail\index.tsx
 * @Description: something
 */
import { View } from "@tarojs/components";
import { useDidShow, useRouter } from "@tarojs/taro";
import { reqCommentReplyByParentId } from "@/api/video";
import React, { useEffect, useState } from "react";
import CommentItem from "@/components/Comment/CommentItem";
import "./index.scss";
import { TC } from "../VideoDetail";
export default function index() {
  const router = useRouter();
  const { typeId, type, paraentId } = router.params;
  const [replys, setReplys] = useState<TC[]>([]);
  useDidShow(() => {
    getReply();
  });
  const getReply = async () => {
    const { data } = await reqCommentReplyByParentId(
      paraentId!,
      typeId!,
      type!
    );
    console.log(data);
    if (data.code === 200) {
      const arr = [...replys, ...data.data.comments];
      setReplys(arr);
    }
  };
  return (
    <View className="comment-detail-container-unique">
      {replys.map(item => {
        return (
          <CommentItem
            commentItem={item}
            type={+type!}
            typeId={+typeId!}
          ></CommentItem>
        );
      })}
    </View>
  );
}
