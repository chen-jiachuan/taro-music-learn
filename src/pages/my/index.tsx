/*
 * @Date: 2022-06-18 13:56:30
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-24 15:35:22
 * @FilePath: \taro-music-learn\src\pages\my\index.tsx
 * @Description: 个人信息
 */
import LoadingCmp from "@/components/LoadingCmp";
import { View } from "@tarojs/components";
import UserDetail from "../packageA/userDetail";
import "./index.scss";
export default function index() {
  return (
    <View>
      <LoadingCmp />
      <UserDetail></UserDetail>
    </View>
  );
}
