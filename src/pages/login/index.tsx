/*
 * @Date: 2022-06-12 13:12:05
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-24 15:35:09
 * @FilePath: \taro-music-learn\src\pages\login\index.tsx
 * @Description: something
 */
import React, { useState } from "react";
import { View, Text, Input } from "@tarojs/components";
import { AtButton, AtForm, AtIcon, AtInput, AtMessage } from "taro-ui";
import { fetchReqLogin } from "@/store/user";
import { useDispatch } from "react-redux";
import "./index.scss";
import Taro from "@tarojs/taro";
import LoadingCmp from "@/components/LoadingCmp";
type InputType = "phone" | "password";
const index = () => {
  const dispatch = useDispatch();
  const [username, setUsername] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const login = () => {
    dispatch<any>(fetchReqLogin({ username, password }));
  };
  const handleChange = (type: InputType, e: any) => {
    // const { value } = e.detail;
    if (type === "phone") {
      setUsername(e);
    } else {
      setPassword(e);
    }
  };
  return (
    <View id="login-container">
      <LoadingCmp />
      <View
        onClick={() => {
          // Taro.navigateBack();
          Taro.redirectTo({
            url: "/pages/index/index"
          });
        }}
      >
        <AtIcon value="chevron-left"></AtIcon>
        返回首页
      </View>
      <AtMessage />
      <Text className="login-title">登录</Text>
      <AtForm className="login-form">
        <AtInput
          title="账号"
          value={username}
          type="text"
          placeholder="输入账号"
          onChange={val => {
            handleChange("phone", val);
          }}
        />
        <AtInput
          title="密码"
          value={password}
          type="password"
          placeholder="输入密码"
          onChange={val => {
            handleChange("password", val);
          }}
        />
        <AtButton
          formType="submit"
          type="primary"
          className="login-btn"
          onClick={login}
        >
          提交
        </AtButton>
      </AtForm>
    </View>
  );
};
export default index;
