/*
 * @Date: 2022-07-01 15:47:36
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-07-05 17:08:05
 * @FilePath: \taro-music-learn\src\components\Comment\CommentItem\index.tsx
 * @Description: something
 */
import { View } from "@tarojs/components";
import React, { useEffect, useState } from "react";
import { AtAvatar, AtIcon } from "taro-ui";
import { ICommentInfo, TC } from "@/pages/packageA/VideoDetail";
import dayjs from "dayjs";
import "./index.scss";
import { reqCommentReplyByParentId } from "@/api/video";
import Taro from "@tarojs/taro";
interface ICommentItem {
  commentItem: TC;
}
const index: React.FC<ICommentItem & {
  type: number;
  typeId: number | string;
}> = ({ typeId, type, commentItem }) => {
  const go2CommentDetail = () => {
    Taro.navigateTo({
      url: `/pages/packageA/CommentDetail/index?typeId=${typeId}&type=${type}&paraentId=${commentItem.commentId}`
    });
  };
  return (
    <View className="comment-item-container-unique">
      <View className="comment-item-header">
        <View className="left-comment-creator-info">
          <AtAvatar
            size="small"
            circle
            image={commentItem.user.avatarUrl + "?param=40y40"}
          ></AtAvatar>
          <View className="right-other-info">
            <View className="comment-creator-name">
              {commentItem.user.nickname}
            </View>
            <View className="publish-time">
              {dayjs(commentItem.time).format("YYYY-MM-DD")}
            </View>
          </View>
        </View>
        <View className="dianzan-btn">
          {commentItem.likedCount}
          <AtIcon value="heart-2"></AtIcon>
        </View>
      </View>
      <View className="comment-item-contaienr">{commentItem.content}</View>
      {commentItem.replyCount ? (
        <View className="show-contaienr-reply" onClick={go2CommentDetail}>
          查看更多{commentItem.replyCount}条回复{">"}
        </View>
      ) : (
        true
      )}
    </View>
  );
};
export default index;
