/*
 * @Date: 2022-07-01 15:24:06
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-07-05 17:03:55
 * @FilePath: \taro-music-learn\src\components\Comment\index.tsx
 * @Description: 评论显示组件
 */
import { View, Image } from "@tarojs/components";
import React, { useState } from "react";
import { AtButton, AtFloatLayout, AtInput, AtTextarea } from "taro-ui";
import CommentItem from "./CommentItem";
import "./index.scss";
import LoadingCmp from "@/components/LoadingCmp";
import { TC, ICommentInfo } from "@/pages/packageA/VideoDetail";
export interface IComment {
  isOpened: boolean;
  comments: TC[];
  total: number;
  type: number;
  typeId: number | string;
  onClose: () => void;
  onScrollToLower?: () => void;
}
const index: React.FC<IComment> = ({
  isOpened,
  comments,
  total,
  onClose,
  onScrollToLower,
  type,
  typeId
}) => {
  const [content, setContent] = useState("");
  const contentChange = value => {
    setContent(value);
  };
  return (
    <>
      <AtFloatLayout
        scrollY
        className="comment-container-unique"
        isOpened={isOpened}
        onScrollToLower={onScrollToLower}
        onClose={onClose}
      >
        <LoadingCmp></LoadingCmp>
        <View className="comment-header">
          评论({total})<View className="choose-hot-or-new"></View>
        </View>
        <View style={{ marginTop: "3.125rem" }}>
          {comments.map((item, index) => {
            return (
              <CommentItem
                typeId={typeId}
                type={type}
                key={item.commentId}
                commentItem={item}
              ></CommentItem>
            );
          })}
          <View className="comment-placeholder-container">
            <AtTextarea
              value={content}
              count={false}
              height={50}
              onChange={contentChange}
              placeholder="发条评论支持一下吧~"
            ></AtTextarea>
            <View>
              <AtButton
                type="primary"
                size="small"
                disabled={content.trim() === ""}
              >
                发送
              </AtButton>
            </View>
          </View>
        </View>
      </AtFloatLayout>
    </>
  );
};
export default index;
