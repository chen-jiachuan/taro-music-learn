/*
 * @Date: 2022-06-15 20:48:42
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-29 15:19:22
 * @FilePath: \taro-music-learn\src\components\MusicListItem\index.tsx
 * @Description: 歌单信息
 */
import { Text, View } from "@tarojs/components";
import React from "react";
import { AtAvatar } from "taro-ui";
import "./index.scss";
import { playCountFormat } from "@/utils/index";
import Taro from "@tarojs/taro";
export interface IMusicListItem {
  name: string;
  coverImgUrl: string;
  creator: any;
  playCount: number;
  id: number;
}
export interface IMusicInfo {
  musicInfo: IMusicListItem;
}
const index: React.FC<IMusicInfo> = ({
  musicInfo: { name, coverImgUrl, creator, playCount, id }
}) => {
  const go2Detail = () => {
    Taro.navigateTo({
      url: "/pages/packageA/MusicListDetail/index?id=" + id
    });
  };
  return (
    <View id="music-list-item-unique-ok" onClick={go2Detail}>
      <View className="container">
        <AtAvatar image={coverImgUrl} className="music-img"></AtAvatar>
        <View className="item-right-info">
          <Text className="music-list-name">{name}</Text>
          <Text className="creator-name"> by {creator.nickname}</Text>
          <Text className="creator-name">
            播放{playCountFormat(playCount)}次
          </Text>
        </View>
      </View>
    </View>
  );
};
export default index;
