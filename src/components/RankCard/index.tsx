/*
 * @Date: 2022-06-27 10:01:41
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-27 11:39:19
 * @FilePath: \taro-music-learn\src\components\RankCard\index.tsx
 * @Description: RankCard
 */
import { View } from "@tarojs/components";
import React from "react";
import classNames from "classnames";
import "./index.scss";
type TItem = {
  name: string;
  title: string;
  searchWord: string;
  onClick: (keyword: string) => void;
};
export interface IRankCard {
  title: string;
  itemList: TItem[];
  onClick: (keyword: string) => void;
}
const index: React.FC<IRankCard> = ({ title, itemList, onClick }) => {
  return (
    <View id="rank-card-hot-container-unique">
      <View className="hot-container">
        <View className="hot-title">{title}</View>
        <View className="hot-list-container">
          {itemList.map((item, index) => {
            return (
              <View
                className="list-item"
                key={index}
                onClick={() => {
                  onClick(item.name || item.searchWord || item.title);
                }}
              >
                <View
                  className={classNames({
                    "left-index": true,
                    "is-hot": index + 1 <= 3 ? true : false
                  })}
                >
                  {index + 1}
                </View>
                <View className="right-name">
                  {item.name || item.searchWord || item.title}
                </View>
              </View>
            );
          })}
        </View>
      </View>
    </View>
  );
};
export default index;
