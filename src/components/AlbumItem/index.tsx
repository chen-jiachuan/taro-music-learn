/*
 * @Date: 2022-06-29 11:03:38
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-29 11:52:19
 * @FilePath: \taro-music-learn\src\components\AlbumItem\index.tsx
 * @Description: AlbumItem
 */
import { View, Image } from "@tarojs/components";
import React from "react";
import "./index.scss";
import dayjs from "dayjs";
type TAlbumItem = {
  name: string;
  publishTime: number;
  artist: { name: string; id: number };
  picUrl: string;
};
export interface IAlbumItem {
  albumInfo: TAlbumItem;
}
const index: React.FC<IAlbumItem> = ({ albumInfo }) => {
  return (
    <View id="album-item-container-unique">
      <View className="left-pic-box">
        <Image mode="aspectFit" src={albumInfo.picUrl} className="img"></Image>
      </View>
      <View className="right-album-info">
        <View className="album-name">{albumInfo.name}</View>
        <View className="singer-publish">
          <View className="album-singer">{albumInfo.artist.name}</View>
          <View className="album-publish-time">
            {dayjs(albumInfo.publishTime).format("YYYY-MM-DD")}
          </View>
        </View>
      </View>
    </View>
  );
};
export default index;
