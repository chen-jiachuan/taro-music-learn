/*
 * @Date: 2022-06-23 14:11:41
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-30 10:12:41
 * @FilePath: \taro-music-learn\src\components\MusicInfoItem\index.tsx
 * @Description: something
 */
import { View } from "@tarojs/components";
import React from "react";
import "./index.scss";
import { fetchSongDetailInfo, TPlaySongInfo } from "@/store/playSong";
import { useSelector } from "@/store/hooks";
import classNames from "classnames";
import { AtIcon } from "taro-ui";
import { deleteSongById } from "@/store/playSong";
import { useDispatch } from "react-redux";
export interface IMusicInfoItem {
  index: number;
  songInfo: TPlaySongInfo;
  isDelete?: boolean;
}
const index: React.FC<IMusicInfoItem> = ({ index, songInfo, isDelete }) => {
  const { currentSongState } = useSelector(state => ({
    currentSongState: state.PlaySong
  }));
  const dispatch = useDispatch();
  const deleteSong = e => {
    e.stopPropagation();
    console.log("歌曲id", songInfo.id);
    dispatch<any>(deleteSongById(songInfo.id));
  };
  const playSong = e => {
    e.stopPropagation();
    dispatch<any>(fetchSongDetailInfo(songInfo.id));
  };
  return (
    <View id="music-info-item-container-unique">
      <View
        className={classNames({
          "music-info-item-container": true,
          "is-current": currentSongState.currentSong.id === songInfo.id
        })}
        onClick={e => {
          playSong(e);
        }}
      >
        <View className="left-index">
          {currentSongState.currentSong.id === songInfo.id ? (
            <AtIcon value="volume-plus" color="#F00"></AtIcon>
          ) : index >= 10 ? (
            index
          ) : (
            "0" + index
          )}
        </View>
        <View className="right-song-info">
          <View
            className="song-name"
            style={{
              color:
                currentSongState.currentSong.id === songInfo.id
                  ? "#ff0000"
                  : "#000"
            }}
          >
            {songInfo.name}
          </View>
          <View className="singer">{songInfo.ar && songInfo.ar[0].name}</View>
        </View>
        {isDelete ? (
          <View
            className="delete-btn"
            onClick={e => {
              deleteSong(e);
            }}
          >
            <AtIcon value="close" size={15}></AtIcon>
          </View>
        ) : (
          true
        )}
      </View>
    </View>
  );
};
index.defaultProps = {
  songInfo: {
    id: 0,
    name: "",
    ar: [],
    al: {
      picUrl: "",
      name: ""
    },
    url: "",
    dt: 0, // 总时长，ms
    st: 0 // 是否喜欢
  },
  isDelete: false,
  index: -1
};
export default index;
