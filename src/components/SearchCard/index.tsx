/*
 * @Date: 2022-06-29 10:14:54
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-07-05 16:51:59
 * @FilePath: \taro-music-learn\src\components\SearchCard\index.tsx
 * @Description: searchCard
 */
import { View } from "@tarojs/components";
import Taro from "@tarojs/taro";
import React, { ReactElement, useContext } from "react";
import { AtIcon } from "taro-ui";
import "./index.scss";
export interface ISearchCard {
  title: string;
  bottomCountText: string;
  renderProps?: () => ReactElement;
}
const index: React.FC<ISearchCard> = ({
  title,
  bottomCountText,
  renderProps
}) => {
  const clickBottomHandler = (data: string) => {
    Taro.eventCenter.trigger("clickBottom", data);
  };
  return (
    <View className="search-card-container-unique">
      <View className="search-card-hedaer">
        <View className="title">{title}</View>
      </View>
      <View className="search-card-container">
        {renderProps && renderProps()}
      </View>
      <View
        className="search-card-bottom"
        onClick={() => {
          clickBottomHandler(title);
        }}
      >
        {bottomCountText}
        <AtIcon value="chevron-right" size={15}></AtIcon>
      </View>
    </View>
  );
};
export default index;
