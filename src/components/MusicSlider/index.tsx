/*
 * @Date: 2022-06-19 11:09:59
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-19 12:58:56
 * @FilePath: \my-taro-app\src\components\MusicSlider\index.tsx
 * @Description: MusicSlider
 */
import { Slider, View } from "@tarojs/components";
import React from "react";
import "./index.scss";
export interface IMusicSlider {
  progress: number;
  onChange: (object) => any;
  onChanging: (object) => any;
}
const index: React.FC<IMusicSlider> = ({ progress, onChange, onChanging }) => {
  return (
    <View id="slider-container-unique">
      <Slider
        blockSize={12}
        activeColor="#d43c33"
        value={progress}
        onChange={e => {
          onChange(e);
        }}
        onChanging={e => {
          onChanging(e);
        }}
      ></Slider>
    </View>
  );
};
export default index;
