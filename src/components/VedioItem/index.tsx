/*
 * @Date: 2022-06-29 14:16:06
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-07-01 11:21:16
 * @FilePath: \taro-music-learn\src\components\VedioItem\index.tsx
 * @Description: VedioItem
 */
import { View, Image } from "@tarojs/components";
import React from "react";
import "./index.scss";
import { fetchCreatorInfoById } from "@/store/video";
import { TimeHandler, playCountFormat } from "@/utils/index";
import { AtIcon } from "taro-ui";
import Taro from "@tarojs/taro";
import { updateVideoTitle } from "@/store/video";
import { useDispatch } from "react-redux";
type creatorItem = {
  userName: string;
  userId: number;
};
type TVedioItem = {
  coverUrl: string;
  title: string;
  creator: Array<creatorItem>;
  playTime: number;
  durationms: number;
  vid: string;
  type: 0 | 1; //0:mv 1:video
};
export interface IVedioItem {
  videoInfo: TVedioItem;
}
const index: React.FC<IVedioItem> = ({ videoInfo }) => {
  const dispatch = useDispatch();
  const go2VideoDetail = () => {
    dispatch(updateVideoTitle(videoInfo.title));
    dispatch<any>(fetchCreatorInfoById(videoInfo.creator[0].userId));
    Taro.navigateTo({
      url: `/pages/packageA/VideoDetail/index?vid=${videoInfo.vid}&type=${videoInfo.type}`
    });
  };
  return (
    <View id="vedio-item-container-unique">
      <View className="vedio-item-container" onClick={go2VideoDetail}>
        <View className="left-video-cover">
          <Image src={videoInfo.coverUrl} className="img"></Image>
          <AtIcon value="video" className="play-icon"></AtIcon>
        </View>
        <View className="right-video-info">
          <View className="title">{videoInfo.title}</View>
          <View className="video-info ">
            {`${TimeHandler(videoInfo.durationms)}, ${videoInfo.creator &&
              videoInfo.creator[0].userName}, ${playCountFormat(
              videoInfo.playTime
            )}次播放`}
          </View>
        </View>
      </View>
    </View>
  );
};
export default index;
