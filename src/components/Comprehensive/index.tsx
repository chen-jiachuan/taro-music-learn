/*
 * @Date: 2022-06-28 15:27:30
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-07-01 09:13:07
 * @FilePath: \taro-music-learn\src\components\Comprehensive\index.tsx
 * @Description: Comprehensive
 */
import { View } from "@tarojs/components";
import { useDidShow } from "@tarojs/taro";
import React, { useEffect } from "react";
import MusicInfoItem from "@/components/MusicInfoItem";
import MusicListItem from "@/components/MusicListItem";
import AlbumItem from "@/components/AlbumItem";
import VideoItem from "@/components/VedioItem";
import SingerItem from "@/components/SingerItem";
import SearchCard from "@/components/SearchCard";
import { useDispatch } from "react-redux";
import {
  fetchSearchSongData,
  fetchSearchAlbumData,
  fetchSearchPlayListData,
  fetchSearchSingerData,
  fetchSearchVideoData,
  clearSearchData
} from "@/store/classification";
import "./index.scss";
import { useSelector } from "@/store/hooks";
export interface IComprehensive {
  keyword: string;
}
const index: React.FC<IComprehensive> = ({ keyword }) => {
  const dispatch = useDispatch();
  const {
    searchSongs,
    searchPlaylist,
    searchAlbums,
    searchVideos,
    searchSingers
  } = useSelector(state => ({
    searchSongs: state.Classification.searchSongs,
    searchPlaylist: state.Classification.searchPlayLists,
    searchAlbums: state.Classification.searchAlnums,
    searchVideos: state.Classification.searchVideos,
    searchSingers: state.Classification.searchArtists
  }));
  useEffect(() => {
    dispatch(clearSearchData());
    dispatch<any>(
      fetchSearchSongData({
        keywords: keyword,
        offset: 1,
        type: 1
      })
    );
    dispatch<any>(
      fetchSearchAlbumData({
        keywords: keyword,
        offset: 1,
        type: 10
      })
    );
    dispatch<any>(
      fetchSearchPlayListData({
        keywords: keyword,
        offset: 1,
        type: 1000
      })
    );
    dispatch<any>(
      fetchSearchSingerData({
        keywords: keyword,
        offset: 1,
        type: 100
      })
    );
    dispatch<any>(
      fetchSearchVideoData({
        keywords: keyword,
        offset: 1,
        type: 1014
      })
    );
  }, []);
  //歌曲渲染
  const songsRender = () => {
    return (
      <View>
        {searchSongs.songs.slice(0, 5).map((item, index) => {
          return (
            <View
              style={{
                borderBottom: "1px solid #f3f3f3",
                padding: "0.625rem 0"
              }}
            >
              <MusicInfoItem songInfo={item} index={index + 1}></MusicInfoItem>
            </View>
          );
        })}
      </View>
    );
  };
  //歌单渲染
  const playListRender = () => {
    return (
      <View>
        {searchPlaylist.playlists.slice(0, 5).map((item, index) => {
          return (
            <View
              style={{
                padding: "0.625rem 0"
              }}
            >
              <MusicListItem musicInfo={item}></MusicListItem>
            </View>
          );
        })}
      </View>
    );
  };
  const albumRender = () => {
    return (
      <View>
        {searchAlbums.albums.slice(0, 5).map((item, index) => {
          return (
            <View
              style={{
                padding: "0.625rem 0"
              }}
            >
              <AlbumItem albumInfo={item}></AlbumItem>
            </View>
          );
        })}
      </View>
    );
  };
  const singerRender = () => {
    return (
      <View>
        {searchSingers.artists.slice(0, 5).map(item => {
          return (
            <View
              key={item.id}
              style={{
                padding: "0.625rem 0"
              }}
            >
              <SingerItem singerInfo={item}></SingerItem>
            </View>
          );
        })}
      </View>
    );
  };
  const videoRender = () => {
    return (
      <View>
        {searchVideos.videos.slice(0, 5).map((item, index) => {
          return (
            <View
              style={{
                padding: "0.625rem 0"
              }}
            >
              <VideoItem videoInfo={item}></VideoItem>
            </View>
          );
        })}
      </View>
    );
  };
  return (
    <View id="comprehensive-container-unique">
      <View style={{ marginBottom: "5rem" }}>
        <SearchCard
          title="歌手"
          bottomCountText={`查看全部${searchSingers.artistCount}个相关歌手`}
          renderProps={singerRender}
        ></SearchCard>
        <SearchCard
          title="单曲"
          bottomCountText={`查看全部${searchSongs.songCount}首相关单曲`}
          renderProps={songsRender}
        ></SearchCard>
        <SearchCard
          title="歌单"
          bottomCountText={`查看全部${searchPlaylist.playlistCount}个相关歌单`}
          renderProps={playListRender}
        ></SearchCard>
        <SearchCard
          title="专辑"
          bottomCountText={`查看全部${searchAlbums.albumCount}个相关专辑`}
          renderProps={albumRender}
        ></SearchCard>
        <SearchCard
          title="视频"
          bottomCountText={`查看全部${searchVideos.videoCount}个相关视频`}
          renderProps={videoRender}
        ></SearchCard>
      </View>
    </View>
  );
};
export default index;
