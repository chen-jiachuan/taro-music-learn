/*
 * @Date: 2022-06-29 15:38:38
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-29 15:50:34
 * @FilePath: \taro-music-learn\src\components\SingerItem\index.tsx
 * @Description: SingerItem
 */
import { View } from "@tarojs/components";
import React from "react";
import { AtAvatar, AtButton } from "taro-ui";
import "./index.scss";
type TSingerItem = {
  picUrl: string;
  name: string;
};
export interface ISingerItem {
  singerInfo: TSingerItem;
}
const index: React.FC<ISingerItem> = ({ singerInfo }) => {
  return (
    <View id="singer-item-container-unique">
      <View className="singer-item">
        <View className="singer-info">
          <AtAvatar
            image={singerInfo.picUrl + "?param=50y50"}
            circle
          ></AtAvatar>
          <View className="singer-name">{singerInfo.name}</View>
        </View>
        <View className="sub-btn">
          <AtButton type="secondary" size="small" circle>
            关注
          </AtButton>
        </View>
      </View>
    </View>
  );
};
export default index;
