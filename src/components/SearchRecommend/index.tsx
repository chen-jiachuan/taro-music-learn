/*
 * @Date: 2022-06-28 11:02:08
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-28 15:22:59
 * @FilePath: \taro-music-learn\src\components\SearchRecommend\index.tsx
 * @Description: SearchRecommend
 */
import { View, Text } from "@tarojs/components";
import React from "react";
import { AtIcon } from "taro-ui";
import "./index.scss";
export interface Ibase {
  name: string;
  id: number;
}
interface TAlbums extends Ibase {
  artist: { id: number; name: string };
}
interface TArtists extends Ibase {}
interface TPlaylists extends Ibase {}
interface TSongs extends Ibase {
  artists: { id: number; name: string }[];
}
export interface ISearchRecommend {
  searchInfo: {
    albums: TAlbums[];
    artists: TArtists[];
    playlists: TPlaylists[];
    songs: TSongs[];
  };
  clickItem: (param: any) => void;
}
const index: React.FC<ISearchRecommend> = ({ searchInfo, clickItem }) => {
  return (
    <View id="search-recommend-container-unique">
      <View className="recommend-search-list">
        <View className="recommend-search-album search-item">
          <View className="header-title">
            <AtIcon value="calendar" color="#ff0000"></AtIcon>单曲
          </View>
          <View className="list-container">
            {searchInfo.songs &&
              searchInfo.songs.map((item, index) => {
                return (
                  <View
                    className="keyword-info"
                    key={item.id}
                    onClick={() => {
                      clickItem(item);
                    }}
                  >
                    <View className="left-index">{index + 1}</View>
                    {item.name} -
                    <Text className="artist">{item.artists[0].name}</Text>
                  </View>
                );
              })}
          </View>
        </View>
        <View className="recommend-search-song search-item">
          <View className="header-title">
            <AtIcon value="file-audio" color="#ff0000"></AtIcon>歌手
          </View>
          <View className="list-container">
            {searchInfo.artists &&
              searchInfo.artists.map((item, index) => {
                return (
                  <View
                    className="keyword-info"
                    key={item.id}
                    onClick={() => {
                      clickItem(item);
                    }}
                  >
                    <View className="left-index">{index + 1}</View>
                    {item.name}
                  </View>
                );
              })}
          </View>
        </View>
        <View className="recommend-search-singer search-item">
          <View className="header-title">
            <AtIcon value="user" color="#ff0000"></AtIcon>专辑
          </View>
          <View className="list-container">
            {searchInfo.albums &&
              searchInfo.albums.map((item, index) => {
                return (
                  <View
                    className="keyword-info"
                    key={item.id}
                    onClick={() => {
                      clickItem(item);
                    }}
                  >
                    <View className="left-index">{index + 1}</View>
                    {item.name}-
                    <Text className="artist">{item.artist.name}</Text>
                  </View>
                );
              })}
          </View>
        </View>
        <View className="recommend-search-musciList search-item">
          <View className="header-title">
            <AtIcon value="align-center" color="#ff0000"></AtIcon>歌单
          </View>
          <View className="list-container">
            {searchInfo.playlists &&
              searchInfo.playlists.map((item, index) => {
                return (
                  <View
                    className="keyword-info"
                    key={item.id}
                    onClick={() => {
                      clickItem(item);
                    }}
                  >
                    <View className="left-index">{index + 1}</View>
                    {item.name}
                  </View>
                );
              })}
          </View>
        </View>
      </View>
    </View>
  );
};
export default index;
