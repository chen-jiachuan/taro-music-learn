/*
 * @Date: 2022-06-25 17:28:57
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-25 18:22:50
 * @FilePath: \my-taro-app\src\components\GoTop\index.tsx
 * @Description: GoTop
 */
import { View } from "@tarojs/components";
import React, { useState } from "react";
import classNames from "classnames";
import { AtIcon } from "taro-ui";
import "./index.scss";
import { usePageScroll } from "@tarojs/taro";
import Taro from "@tarojs/taro";
function index() {
  const [show, setShow] = useState(false);
  const [isFrist, setIsFrist] = useState(false);
  usePageScroll(res => {
    if (res.scrollTop > 500) {
      setShow(true);
      setIsFrist(true);
    } else {
      setShow(false);
    }
  });
  const goTop = e => {
    e.stopPropagation();
    console.log(12313123);
    Taro.pageScrollTo({
      scrollTop: 0,
      duration: 1000
    });
  };
  return (
    <View id="go-top-container-unique">
      {isFrist ? (
        <View
          onClick={e => {
            goTop(e);
          }}
          className={classNames({
            "go-top-container": true,
            "show-btn": show,
            "hide-btn": !show
          })}
        >
          <AtIcon value="chevron-up"></AtIcon>
        </View>
      ) : (
        true
      )}
    </View>
  );
}
export default index;
