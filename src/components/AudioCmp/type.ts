/*
 * @Date: 2022-06-18 15:12:23
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-18 15:21:24
 * @FilePath: \my-taro-app\src\components\AudioCmp\type.ts
 * @Description: something
 */
export type TPlaySongInfo = {
  id: number;
  name: string;
  ar: Array<{
    name: string;
  }>;
  al: {
    picUrl: string;
    name: string;
  };
  url: string;
  lrcInfo: any;
  dt: number; // 总时长，ms
  st: number; // 是否喜欢}
};
export type MusicItemType = {
  name: string;
  id: number;
  ar: Array<{
    name: string;
  }>;
  al: {
    name: string;
  };
  song: {
    id: number;
  };
  copyright: number;
  st?: number;
  current?: boolean;
};
