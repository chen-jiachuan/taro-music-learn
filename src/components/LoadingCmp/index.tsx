/*
 * @Date: 2022-06-24 14:49:28
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-25 18:46:34
 * @FilePath: \my-taro-app\src\components\LoadingCmp\index.tsx
 * @Description: LoadingCmp
 */
import { FC, memo } from "react";
import classnames from "classnames";
import { View } from "@tarojs/components";
import "./index.scss";
import { useSelector } from "@/store/hooks";
type Props = {
  fullPage?: boolean;
  hide?: boolean;
};

const CLoading: FC<Props> = () => {
  const { LoadingState } = useSelector(state => ({
    LoadingState: state.Loading
  }));
  const cls = classnames({
    loading_components: true,
    hide: !LoadingState.is_loading
  });
  return <View className={cls}></View>;
};

export default CLoading;
