/*
 * @Date: 2022-06-14 16:26:29
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-07-01 15:10:03
 * @FilePath: \taro-music-learn\src\components\ImgListShow\index.tsx
 * @Description: 图片组件
 */
import React, { memo } from "react";
import {
  View,
  Button,
  Swiper,
  SwiperItem,
  Image,
  Text
} from "@tarojs/components";
import { AtIcon } from "taro-ui";
import { playCountFormat } from "@/utils";
import "./index.scss";
import Taro from "@tarojs/taro";
type imgItem = {
  picUrl: string;
  name: string;
  id: number;
  playCount: number;
};
export interface IImgListShow {
  title: string;
  imgList: imgItem[];
}
const index: React.FC<IImgListShow> = memo(({ title, imgList }) => {
  const go2MusicListDetail = (id: number) => {
    Taro.navigateTo({
      url: "/pages/packageA/MusicListDetail/index?id=" + id
    });
  };
  return (
    <View id="img-list-show-container">
      <View className="img-list-show-header">
        <View className="title">{title}</View>
        <Button className="detail-btn" type="warn">
          更多 <AtIcon value="chevron-right" size="10"></AtIcon>
        </Button>
      </View>
      <View className="img-list">
        {imgList.slice(0, 10).map(item => {
          return (
            <>
              <View
                className="item-container"
                key={item.id}
                onClick={() => go2MusicListDetail(item.id)}
              >
                <Image className="img" lazyLoad src={item.picUrl}></Image>
                <View className="name">{item.name}</View>
                <View className="playCount">
                  <AtIcon value="play" size={10}></AtIcon>
                  {playCountFormat(item.playCount)}
                </View>
              </View>
            </>
          );
        })}
      </View>
    </View>
  );
});
index.defaultProps = {
  title: "",
  imgList: []
};
export default index;
