/*
 * @Date: 2022-06-22 11:27:04
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-22 16:28:19
 * @FilePath: \taro-music-learn\src\components\RotatePic\index.tsx
 * @Description: 旋转图片 needle-pic needleRotate
 */
import { View, Image } from "@tarojs/components";
import React, { useEffect } from "react";
import { TPlaySongInfo } from "@/store/playSong";
import classNames from "classnames";
import { useSelector } from "@/store/hooks";
import "./index.scss";
const disc = require("../../assets/images/disc.png");
const needlePng = require("../../assets/images/needle.png");
export interface IRotatePic {
  currentSongInfo: TPlaySongInfo;
}
const index: React.FC<IRotatePic> = ({ currentSongInfo }) => {
  const { currentSongState } = useSelector(state => ({
    currentSongState: state.PlaySong
  }));
  return (
    <View id="rotate-pic-container-unique">
      <Image
        src={needlePng}
        className={classNames({
          "needle-pic": true,
          "needle-rotate": currentSongState.isPlaying
        })}
      ></Image>
      <View
        className={classNames({
          "song-music-img": true,
          "disc-animation": currentSongState.isPlaying,
          "animation-pause": !currentSongState.isPlaying,
          "animation-play": currentSongState.isPlaying
        })}
      >
        <Image src={disc} className="disc-img"></Image>
        <Image
          src={currentSongInfo.al.picUrl}
          className="song-pic-cover"
        ></Image>
      </View>
    </View>
  );
};
export default index;
