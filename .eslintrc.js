/**
 * @Date: 2022-06-11 21:14:48
 * @LastEditors: sailornpg
 * @LastEditTime: 2022-06-18 12:39:00
 * @FilePath: \my-taro-app\.eslintrc.js
 * @Description: something
 */
module.exports = {
  extends: ["taro/react"],
  rules: {
    "react/jsx-uses-react": "off",
    "react/react-in-jsx-scope": "off",
    "react/jsx-filename-extension": [1, { extensions: [".js", ".jsx", ".tsx"] }]
  },
  parser: "babel-eslint",
  plugins: ["typescript"]
};
